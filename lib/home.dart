import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:transparent_image/transparent_image.dart';

class MyHomePage extends StatefulWidget {
  final String title;

  // class constructor
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Uint8List? _imageData;

  Widget _getSelectedImage() {
    if (_imageData != null) {
      return FadeInImage(
          placeholder: MemoryImage(kTransparentImage),
          image: MemoryImage(_imageData!));
    } else {
      return const Text('No image selected');
    }
  }

  void _pickImage() async {
    // TODO: implement multiple photos
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(source: ImageSource.gallery);
    final Uint8List imageBytes = await image!.readAsBytes();
    setState(() {
      _imageData = imageBytes;
    });
  }

  // rerun every time setState() is called
  @override
  Widget build(BuildContext context) {
    Widget imageWidget = _getSelectedImage();

    return Scaffold(
      body: Center(
        child: imageWidget,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _pickImage,
        tooltip: 'Select an image file',
        child: const Icon(Icons.add),
      ),
    );
  }
}
